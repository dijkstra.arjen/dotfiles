return {
  {
    "https://github.com/Ramilito/kubectl.nvim",
    config = function()
      require("kubectl").setup()
    end,
  },
}
